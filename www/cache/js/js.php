<?php
/*
 * **NOTE** IF EXTERNAL FILE IS ADDED, BUT NO LOCAL FILES ARE CHANGE IT WILL NOT BE
 * ADDED UNTIL YOU MANUALLY REMOVE THE CACHE.
 */

	// Sanity
		chdir(__DIR__);
	// Determine the requested files
		switch (isset($_GET['x']) ? strtolower($_GET['x']) : NULL) {
			case 'dog':
				$cache_filename = '../cache/dog.js';
				$javascripts = array(
					'../../js-plugins/angular/angular.min.js',
					'../../js-plugins/angular/animate.min.js',
					'../../js-plugins/angular/aria.min.js',
					'../../js-plugins/angular/material.min.js',
					'../../js-plugins/angular/messages.min.js',
					'../../js-plugins/angular/ui-router.min.js',
					'../../js/app.js',
					'../../js/misc/router.js',
					'../../js/index.controller.js'
				);
				break;
			
			default:
				$cache_filename = '../cache/default.js';
				$javascripts = [];
				break;
		}
 
	// Determine whether requested files are newer than cache
		$flag_must_recache = true;
		if (file_exists($cache_filename)) {
			$cache_filemtime = filemtime($cache_filename);

			$flag_must_recache = false;
			foreach($javascripts as $javascript) {
				if ( filemtime($javascript) > $cache_filemtime) {
					$flag_must_recache = true;
					break;
				}
			}
		}

	// Overwrite and append requested files to cache when outdated
		if ($flag_must_recache) {
			$cache_fh = fopen($cache_filename, 'w+');
			foreach($javascripts as $javascript) {
				fwrite($cache_fh, file_get_contents($javascript).PHP_EOL);
			}
			fclose($cache_fh);
		}

	// Set HTTP 304 header and exit if client cached copy is correct
		$apache_request_headers = apache_request_headers();
		if (isset($apache_request_headers['If-Modified-Since'])) {
			if (strtotime($apache_request_headers['If-Modified-Since']) >= filemtime($cache_filename)) {
				if (!headers_sent()) {
					header('HTTP/1.1 304 Not Modified', true, 304);
					header('Connection: close');
					exit;
				}
			}
		}
		if (isset($apache_request_headers['If-None-Match'])) {
			if ($apache_request_headers['If-None-Match'] == hash_file('md5', $cache_filename)) {
				if (!headers_sent()) {
					header('HTTP/1.1 304 Not Modified', true, 304);
					header('Connection: close');
					exit;
				}
			}
		}

	// Otherwise set headers for output
		if (!headers_sent()) {
			header('Content-type: text/javascript', true, 200);
			header('ETag: '.hash_file('md5', $cache_filename), true);
			header('Last-modified: '.gmdate('D, d M Y H:i:s T', filemtime($cache_filename)), true);
			header('Cache-control: must-revalidate', true);
		}

	// Compress and output cache from disk
		ob_start('ob_gzhandler') || ob_start();
		readfile($cache_filename);
		ob_end_flush();

?>
