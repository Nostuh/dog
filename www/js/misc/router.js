(function () {
	'use strict';
  	angular
		.module('app')
		.config(Router);
		
	Router.$inject = ['$stateProvider','$urlRouterProvider','$locationProvider'];
	
	function Router($stateProvider, $urlRouterProvider, $locationProvider){
			//Fix Hash Tag stuff
			$locationProvider.html5Mode({enabled: true});
					    
		    $urlRouterProvider.otherwise('/');
		    
		    $stateProvider
		        .state('/', {
		            url: '/',
		            templateUrl: '/pages/content/index.html'
		        })
				
		        .state('services', {
		            url: '/services',
		            templateUrl: '/pages/content/services.html'
		        })
		       	.state('contact', {
		            url: '/contact',
		            templateUrl: '/pages/content/contact.html'
		        })
				/*
		        .state('sinknet_tasks', {
		            url: '/fecta/sinknet_tasks/{id}',
		            templateUrl: '/fecta/pages/sinknet_tasks.html'
		        })
		        .state('accessory_goals', {
		            url: '/fecta/accessory_goals/{id}',
		            templateUrl: '/fecta/pages/accessory_goals.html'
		        });*/
	};
})();
